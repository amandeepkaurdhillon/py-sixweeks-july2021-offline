ls = [
    [87,8, 3],
    [45,43,1],
    [5,66,3],
]

## Transpose of given list 'ls' with and without list comprehension

''''
Transposed matrix:

[
    [87,45,5],
    [8,43,66],
    [3,1,3]
]


'''