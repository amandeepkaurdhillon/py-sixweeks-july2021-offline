def square(a):
    return a**2 

# print(square(6))
# print(square(8))

sq = lambda a:a**2
print(sq(7))

### write a lambda statement to add 3 numbers
r = lambda a,b,c:a+b+c
print(r(4,5,63))