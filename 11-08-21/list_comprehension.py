# ls = []
# for i in range(1,101):
#     if i%8==0:
#         ls.append(i)
# print(ls)
marks = [23,4,65,34,10,8,80,110,78]

lc = [i for i in range(1,101)]
ab = ["hello" for i in range(10)]
div_by_8 = [i for i in range(1, 101) if i%8==0]

result = ["pass" if i>=33 else "fail" for i in marks]
print(result)

matrix = [[j for j in range(4)] for i in range(3)]

for i in matrix:
    print(i)   