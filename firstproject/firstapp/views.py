from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse("<h1>FIRST DJANGO FUNCTION</h1>")

def about(xyz):
    return HttpResponse("<h1 style='color:green'>Welcome To About Page</h1>")

def home(request):  
    colors = ["red","green","blue","magenta","black","yellow","#00ff00"]
    context = {'col':colors}
    return render(request,"home.html",context)