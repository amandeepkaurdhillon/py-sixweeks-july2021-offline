print("Hello World!")
#Python data types 
a = 10
b = 10.5
c = True 
d = [10,20,30] #mutable
e = {'name':'aman','rno':1}
f = 'hello'
g = (10,20,30) #immutable
h = {10,20}
i = None
print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))
print(type(h))
print(type(i))