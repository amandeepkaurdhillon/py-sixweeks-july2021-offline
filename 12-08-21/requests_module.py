#pip install requests
import requests

API = "https://restcountries.eu/rest/v2/all"

data = requests.get(API).json()
# print(data)
name = input("Enter Country name: ")
found =False
for i in data:
    if i["name"].lower() == name.lower():
        found=True
        print(f"\tName: {i['name']}")
        print(f"\tCapital: {i['capital']}")
        print(f"\tPopulation: {i['population']}")
        print(f"\tArea: {i['area']}")
        break
if found==False:
    print(f"'{name}' not found in our database!")