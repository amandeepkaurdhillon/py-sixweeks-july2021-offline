st = 'Welcome to my Site'

print(st)
print(st.upper())
print(st.lower())
print(st.title())
print(st.capitalize())
print(st.swapcase())

if "ABdrgr".isupper():
    print('String is in upper case')
else:
    print("String is not in uppercase")

# print(dir('679080'))

print('69809'.isdigit())
print('69806798ghjvgujuyj9'.isdigit())
print('fhjgj'.isalpha())
print('fhjgj8979'.isalnum())
