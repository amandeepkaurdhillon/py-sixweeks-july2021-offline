intro = "Python was createb by Guido Van Rossum"

print(intro)

#indexing
print(intro[0])
print(intro[-2])

# intro[0]='a' ## py string is immutable

#slicing 
print(intro[2:])
print(intro[:2])
print(intro[2:8])

#Stepping
n = '0123456789'
print(intro[::1])
print(n[::2])
print(n[::-1])

#membership 
print('w' in n)
print('w' not in n)
print('Was' in intro)

#iteration
print('a'*10)
print('2'*10)

for i in range(10):
    print(i*'*')

name = input("Enter Name: ")
print(f"Welcome {name}!!!")