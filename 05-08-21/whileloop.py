# i = 1

# while i<=10:
#     print(i)
#     i+=1

i = 10
while i>=1:
    print(i)
    i-=1

#Access list elementrs using while loop
ls = ['Pyhton','PHP','Ruby','.NET','Java','C++','JavaScript']
# print(ls[0])
# print(ls[1])
# print(ls[2])

index=0
while index<len(ls):
    # print("Index: {}, Value:{}".format(index, ls[index]))
    # print(f"Index: {index}, Value:{ls[index]}")
    print("Index: %d, Value:%s"%(index, ls[index]))
    index+=1