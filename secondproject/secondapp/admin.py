from django.contrib import admin
from secondapp.models import Employee

admin.site.site_header = "MyWebsie | Admin"

class EmployeeAdmin(admin.ModelAdmin):
    # fields = ['name','email','gender']
    list_display = ['id','name','registered_on','email','is_registered']
    list_filter = ['name','registered_on']
    list_editable = ['name','is_registered']
    search_fields = ['name','email']

admin.site.register(Employee, EmployeeAdmin)