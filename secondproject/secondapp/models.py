from django.db import models

gen = (
    ('M','Male'),
    ('F','Female'),
)
class Employee(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    gender = models.CharField(choices=gen,max_length=30)
    website = models.URLField(blank=True)
    contact_number = models.IntegerField()
    salary = models.FloatField(default=5000)
    is_registered = models.BooleanField(default=False)
    address = models.TextField()
    date_of_birth = models.DateField(null=True)
    registered_on =models.DateTimeField(auto_now_add=True,null=True)
    updated_on =models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Employee Table"

