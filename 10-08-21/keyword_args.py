def register(**kwargs):
    for key,value in kwargs.items():
        print(f"{key} : {value}")
    print("="*20)

register(name="Aman", roll_no=1)
register(first_name="Peter",last_name="Parker", age=22)