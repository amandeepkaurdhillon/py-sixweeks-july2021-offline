import random
op = """
    Press 1: To create account
    Press 2: To check balance
    Press 3: To Deposit
    Press 4: To withdraw
    Press 0: To exit
"""
print(op)
users = []
def create_account():
    name = input("Enter Your Name: ")
    amt = input("Enter initial Amount: ")
    if amt.isdigit():
        pin = random.randint(1000,9999)
        usr = {'name':name,'balance':float(amt),'pin':pin}
        users.append(usr)
        print(f"Dear {name}, your account created!, Pin Code: {pin}\n")
    else:
        print("Please Enter a valid amount!\n")
def check_balance():
    p = int(input("Enter Pin Code: "))
    flag=False
    for user in users:
        if user["pin"]==p:
            print(f"Welcome {user['name']}")
            print(f"Current Balance: Rs.{user['balance']}/-\n")
            flag=True
            break
    if flag==False:
        print("Sorry, you are not registered with us!\n")

def deposit():
    p = int(input("Enter Pin Code: "))
    flag=False
    for user in users:
        if user["pin"]==p:
            print(f"Welcome {user['name']}")
            amt = input("Enter Amount to deposit: ")
            user["balance"] += float(amt)
            print(f"Your account credited with Rs.{amt}!/-\n")
            flag=True
            break
    if flag==False:
        print("Sorry, you are not registered with us!\n")

def withdraw():
    p = int(input("Enter Pin Code: "))
    flag=False
    for user in users:
        if user["pin"]==p:
            print(f"Welcome {user['name']}")
            amt = input("Enter Amount to withdraw: ")
            if user['balance']>=float(amt):
                user["balance"] -= float(amt)
                print(f"Your account debited with Rs.{amt}/- Current:Rs.{user['balance']}/-\n")
            else:
                print("You don't have sufficient balance!\n")
            flag=True
            break
    if flag==False:
        print("Sorry, you are not registered with us!\n")

while True:
    ch = input("Enter Your choice: ")
    if ch=="1":
        create_account() 
    elif ch=="2":
        check_balance() 
    elif ch=="3":
        deposit() 
    elif ch=="4":
        withdraw() 
    elif ch=="0":
        break 
    else:
        print("Invalid Choice\n")