def area(pi=3.14,r=0):
    print(f"PI:{pi} r:{r} Area:{pi*r**2}")

area(3.14, 10)
area(3.14, 5)
area()
area(r=100)
area(1,2)