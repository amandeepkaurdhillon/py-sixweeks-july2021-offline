colors = ['red','green','cyan','blue']
print('Before:',colors)
# colors.clear()
# print(colors.index('cyan'))
# print(colors.count('white'))
# colors.append('black')
# colors.append(['black','magenta','pink'])
# colors.extend(['black','magenta','pink'])
# print(colors[4][1])
# colors.pop()
# colors.pop(1)
# del colors[2]
# colors.remove('green')
# colors.remove('green2re567')
colors[1] = "orange"
# print(dir(colors))
print('After:',colors)
a = [3,89,4,23,-2,80]
# a.insert(1,7)
a.sort()
a.reverse()
print(a)