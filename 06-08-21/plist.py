ls = ['Python','PHP','HTML','CSS','JS']
print(ls)
#index 
print(ls[1])
#slicing
print(ls[-3:])
print(ls[:-3])
#stepping
print(ls[::-1])
print(ls[::2])
#membership
print("HTML" in ls)
print("HTML" not in ls)
#iteration 
print(ls*2)
print(dir(ls))