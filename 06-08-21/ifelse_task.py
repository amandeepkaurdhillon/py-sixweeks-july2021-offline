while True:
    num = input("Enter Number: ")
    if num=='exit':
        break
    if num.isdigit():
        if len(num)>=10:
            last = num[-3:]
            n_first = len(num[:-3])*'*' 
            print("Number: {}\n".format(n_first+last))
        else:
            print("Number must contain atleast 10 digits!\n")
    else:
        print("Number must contain digits only!\n")