class Student:
    clz = "ABCD College"
    def info(self,name="James"):
        self.n = name
        print("Student class M.F.")

class Account:
    pending_fee = True 
    def fee(self):
        print("Fe detail function of account class")

#multiple inheritance
class Library(Student, Account):
    def __init__(self):
        self.info()
        print(f"Welcome to {self.clz} Library")
        self.pending_fee = False
        if self.pending_fee:
            print("You can't get books! Please clear dues first!")
        else:
            print("Student can issue book!")
s1 = Library()