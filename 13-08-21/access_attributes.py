class Student:
    clz = "XYZ Engg. College"
    def register(self,name):
        self.n = name
        print(f"{name} registered at {self.clz}!\n")

    def fee_details(self,total, pending):
        print(f"Fee Details of {self.n}")
        print(f"Total Fee: Rs.{total}/-")
        print(f"Pending Fee: Rs.{pending}/-\n")

s1 = Student()
s2 = Student()
s1.register("James")
s2.register("Peter Parker")
s2.fee_details(10000,2000)