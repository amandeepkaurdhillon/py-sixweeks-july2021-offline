class Student:
    clz = "ABCD College of Management"
    def intro(self,name="Jack"):
        self.n = name
class Library(Student): #Simple Inheritance
    def book_details(self,bn, isdt):
        print("Book Details of: ", self.n)
        print(f"Book Name: {bn}")
        print(f"Issue Date: {isdt}")   
s1 = Library()
s1.intro()
print(s1.clz)
s1.book_details("Pyhton Programming","10 August")
