# create a class 
class Student:
    clz = "ABCD Engg. College" #d.m.

    def intro(self):
        print(f"Student of {self.clz} collge") #m.f.

#Create an object
s1 = Student()
# Access attributes
print(s1.clz)
s1.intro()