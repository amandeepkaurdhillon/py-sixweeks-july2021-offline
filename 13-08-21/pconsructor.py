class Circle:
    PI = 3.14
    def __init__(self,radius): #constructor
        self.r = radius
    def calc_area(self):
        print(f"r={self.r} area={self.PI*self.r**2}")
    def __del__(self): #destructor
        del self.r
c1 = Circle(10)
c2 = Circle(5)
c3 = Circle(87)
c2.calc_area()