from datetime import datetime as dt
class Student:
    def __init__(self, name, sub=["Python","CC"], fee=1000):
        self.n = name
        self.sub = sub
        self.fee= fee

class Library(Student):
    def issue_book(self,bname):
        print("Name: ", self.n)
        print("Book name: ", bname)
        print("Issued On: ", dt.now())
class Details(Student):
    def intro(self):
        print("\nName: ",self.n)
        print("Fee: Rs.{}/-".format(self.fee))
        print(f"Subjects Total ({len(self.sub)})")
        for count, s in enumerate(self.sub,start=1):
            print(f"\t{count}. {s}")

s1 = Library("Jack")
s1.issue_book("Python Programming")

s2 = Details("James")
s2.intro()