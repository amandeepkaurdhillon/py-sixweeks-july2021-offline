movie = {
    'name':'PK',
    'hero':'Amir Khan',
}

#Insert / update elements
movie['release_date'] = 2014
# movie['name'] = 'Mr. Amir Khan'
movie.update({'hero':'Amir', 'revenue':'200 Crore'})

print('Before: ', movie)
# movie.pop('hero')
# movie.popitem()
del movie['hero']
print("After: ", movie)

print(movie.keys())
print(movie.values())
print(movie.items(),'\n')

for key,value in movie.items():
    print(f"{key} = {value}")

keys = ['name','email','contact','marks','address']
print({}.fromkeys(keys))
print({}.fromkeys(keys,'not set'))