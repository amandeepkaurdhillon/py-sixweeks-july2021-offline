a = 10,20,30
b = ('HTML','CSS','JS','Bootstrap','JQuery','JS','JS')
print(type(a))
#Indexing
print(a[0])
print(a[-1])

#Slicing
print(a[1:])
print(b[:2])
#stepping
print(b[::-1])
print(b[::-2])

print("JS" in a)
print("JS" not in a)
print("JS" in b)

# a[0]=100 #python tuple is immutable
# del a[0]
# print(dir(a))
print(b.index("JS"))
print(b.count("JS"))