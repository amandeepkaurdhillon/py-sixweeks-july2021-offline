def register(name, age, roll_no):
    print(f"Name: {name}")
    print(f"Age: {age}")
    print(f"Roll No.: {roll_no}\n")

#positional arguments
register("Peter", 10, 1)
#keyword arguments
register(roll_no=1, name="Harry", age=18)
register("Potter", 10, roll_no=2)
# register("Jenny", 10, age=2) #error for multiple values