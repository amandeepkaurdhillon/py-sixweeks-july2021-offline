def greet(): #function deninition
    #function body
    '''This is a function to wish good morning!'''
    print("Good Morning, Everyone!!!")

greet() #function call
greet()

print(greet.__doc__) #Access docstring of a function
print(len.__doc__)
print(print.__doc__)