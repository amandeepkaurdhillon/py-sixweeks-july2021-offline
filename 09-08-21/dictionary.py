student = {
    0:"Student Information",
    "name":"Amandeep Kaur",
    "roll_no":1,
    "hobbies":["playing","travelling", "reading books"]}
# student.clear()
print(student)
print(student["name"])
print(student["hobbies"][2])
print(student.get("roll_no"))
print(student.get("email"))
# print(dir(student))

### Looping 
for i in student:
    print(f"{i} : {student[i]}")