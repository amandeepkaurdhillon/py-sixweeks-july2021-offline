To save changes in database structure
    python manage.py makemigrations
    python manage.py migrate 

To create admin User:
    python manage.py createsuperuser

To run server
    python manage.py runserver

Django Users:
    active : can't login to admin panel 
    staff : can login to admin panel but with limited rights 
    superuser : admin/overall rights , can edit delete update anything even can assign duties to staff users