from django.shortcuts import render
from django.http import HttpResponse
import requests

def index(request):
    data = requests.get("https://restcountries.eu/rest/v2/all").json()
    return render(request,"countries.html", {'countries':data})

def gallery(request):
    return render(request,'gallery.html')



