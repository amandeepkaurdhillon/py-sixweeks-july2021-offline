try:
    a = int(input("Enter A Number: "))
    print(a) 
except ValueError as v:
    print("Error!",v) 
else:
    print("Else! No error in try block ") 
finally:
    print("Finally! I will always execute!")