class College:
    name = "ABCD Engg College"
    def __init__(self):
        print("Constructor of parent class! ")
    def abc(self):
        print("M.f. of parent")
class Student(College):
    name = "James"
    def __init__(self):
        print("Constructor of student class!")
        #access parent class's attributes
        print(super().name)
        super().__init__()
        super().abc()
obj = Student()
# print(obj.name)
