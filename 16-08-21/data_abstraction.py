class Employee:
    name = "Mr. Abcd"
    __pin_code ="JHGF67543546576878"

    def __intro(self):
        print(self.name, "is an employee at XYZ Tech.")

class Test(Employee):
    def __init__(self):
        print("Name: ", self.name)
        # self.__intro() ## Can't access private member functions
        # print("Pin code: ", self.__pin_code) ## Can't access private data members
        
obj = Employee()
obj.name="Peter"
print(obj.name)
# print(obj.__pin_code) ## Can't access private data members
ob = Test()