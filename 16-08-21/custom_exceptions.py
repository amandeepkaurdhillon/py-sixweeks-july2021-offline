class MyError(Exception):
    def __init__(self):
        self.msz = "First number must be greater!"
    def __str__(self):
        return self.msz
try:
    n1 = int(input("Enter First Number: "))
    n2 = int(input("Enter Second Number: "))
    if n1>n2:
        print(f"Result: {n1-n2}")
    else:
        raise MyError
except MyError as err:
    print(err)