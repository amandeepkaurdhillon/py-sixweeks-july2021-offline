f = open('table.html','a')
print("Pointer: ", f.tell())
num = int(input("Enter Number: "))
st=""
for i in range(1,11):
    st+=f"<h1 style='color:red;'>{num} x {i} = <span style='color:green;'>{num*i}</span></h1>"

f.write(st+"<hr/>")
print("Pointer: ", f.tell())
f.close()

print("file created!")