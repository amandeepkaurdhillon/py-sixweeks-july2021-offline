import requests 
data = requests.get("https://restcountries.eu/rest/v2/all").json()
con = input("Enter Country Name: ")
res=''
for i in data:
    if i['name'].title()==con.title():
        res+= '<div style="width:30%;margin:auto;padding:20px;box-shadow:0px 0px 10px black;">'
        res+= f'<img src={i["flag"]} style="height:200px;width:100%;">'
        res+= f'<h1>{i["name"]}</h1>'
        res+= f'<h4>Population: {i["population"]}</h4>'
        res+= f'<h4>Area: {i["area"]}</h4>'
        res+= f'<p>Borders: {i["borders"]}</p>'
        res+= '</div>'

path = "countries/{}.html".format(con)
try:
    f = open(path,'x')
    f.write(res)
    f.close()
    print(path,'created successfully!')
except FileExistsError:
    print(f"A file with '{path}' name already exists!")